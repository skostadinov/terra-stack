import {Request, Response} from "express";

export class CoreController {
    /**
     * Render Index
     * @param req
     * @param res
     */
    static renderIndex(req: Request, res: Response): void {
        res.render('modules/core/server/views/Index.jsx', {
            user: req.body.user || null
        });
    }

    /**
     * Server Error
     * @param req
     * @param res
     */
    static renderServerError(req: Request, res: Response): void {
        res.status(500).render('modules/core/server/views/500', {
           error: 'Sorry, dude! Something went wrong!'
        });
    }

    /**
     * Not Found
     * @param req
     * @param res
     */
    static renderNotFound(req: Request, res: Response): void {
        res.status(404).format({
            'text/html': function () {
                res.render('modules/core/server/views/404', {
                    url: req.originalUrl
                });
            },
            'application/json': function () {
                res.json({
                    error: 'Path not found'
                });
            },
            'default': function () {
                res.send('Path not found');
            }
        });
    }
}

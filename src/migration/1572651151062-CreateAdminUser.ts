import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {User} from "../modules/users/server/entity/User";

export class CreateAdminUser1572651151062 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        let user = new User();
        user.firstName = "admin";
        user.lastName = "admin";
        user.email = "admin@admin.admin";
        user.username = "admin";
        user.password = "admin";
        user.hashPassword();
        user.role = "ADMIN";
        const userRepository = getRepository(User);
        await userRepository.save(user);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
